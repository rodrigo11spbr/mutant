const Express = require('express');
const BodyParser = require('body-parser');

const App = Express();

App.use(BodyParser.json());
App.use(BodyParser.urlencoded({ extended: true }));

require('./app/controllers/imports.js')(App);

App.listen(process.env.PORT || 8080, () => console.log('server is running'));