const Express = require('express');
const Request = require("request");
const ApiConfig = require('../config/apiConfig');

const Router = Express.Router();

function dynamicSort(property) {
    var sortOrder = 1;

    if (property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }

    return function (a, b) {
        if (sortOrder == -1) {
            return b[property].localeCompare(a[property]);
        } else {
            return a[property].localeCompare(b[property]);
        }
    }
}

Router.get('/webSites', (req, res) => {
    Request.get(ApiConfig.users, (error, response, body) => {
        if (error) {
            return res.status(500).send({ fromServer: "Internal server error while try to get websites" });
        }

        let users = JSON.parse(body).map(x => x.website);
        return res.send(users);
    });
});

Router.get('/details', (req, res) => {
    Request.get(ApiConfig.users, (error, response, body) => {
        if (error) {
            return res.status(500).send({ fromServer: "internal server error while try to get name, email and company" });
        }

        let users = JSON.parse(body).sort(dynamicSort("name")).map(({ name, email, company }) => ({ name, email, company }));

        return res.send(users);
    });
});

Router.get('/suite', (req, res) => {
    Request.get(ApiConfig.users, (error, response, body) => {
        if (error) {
            return res.status(500).send({ fromServer: 'internal server error while try get address with suite' });
        }
        let users = JSON.parse(body).filter(x => x.address.suite.indexOf(ApiConfig.search));
        return res.send(users);
    });
});

module.exports = App => App.use('/user', Router);