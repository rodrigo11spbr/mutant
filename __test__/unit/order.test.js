function dynamicSort(property) {
    var sortOrder = 1;

    if (property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }

    return function (a, b) {
        if (sortOrder == -1) {
            return b[property].localeCompare(a[property]);
        } else {
            return a[property].localeCompare(b[property]);
        }
    }
}

var users = [
    { "id": 1, "name": "Rodrigo", "lastname": "Prado", "yearsOld": 23, "career": "Backend" },
    { "id": 2, "name": "Carolina", "lastname": "Fernandes", "yearsOld": 21, "career": "UX" }]

describe('order', () => {
    it('alphabetical order ', () => {
        users.sort(dynamicSort("name"));
        expect(users[0].name).toBe("Carolina");
    });
});

describe('select', () => {
    it('not bring everything', () => {
        let result = users.map(x => x.career);

        expect(result[0]).toBe("UX");
    });
});