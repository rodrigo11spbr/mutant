const Request = require("request");
const ApiConfig = require('../../src/app/config/apiConfig');

describe('from api', () => {
    it('should receive json from api', () => {
        Request.get(ApiConfig.users, (error, response, body) => {
            if (error) {
                return console.dir(error);
            }
            let users = JSON.parse(body);

            expect(users).not.toBeNull();
        });
    });
});